## Puchikun.net

This is a mono repo of typscript code for

-   client: static website build from @clairejs/client
-   server: express-based server build from @clairejs/server
-   common models, DTOs, enums

#### ChangeLog:

###### 1.0.8:
- increase note limit to 8191 chars

###### 1.0.7:
- fix post/put note

###### 1.0.6:
- increase note limit to 4095 chars, update claire core and server

###### 1.0.5:

-   update @clairejs/server 1.3.2 to handle http response

###### 1.0.4:

-   open access get many user

###### 1.0.3:

-   remove authorization header in api provider after logout

###### 1.0.2:

-   fix imporant bug allow private note to be shown on getMany

###### 1.0.1:

-   show private note in search result, disable api debug log

###### 1.0.0:

-   basic note & tag functions
