import { AbstractBootstrap, Inject } from "@clairejs/core";
import { AbstractViewHandler } from "@clairejs/client";
import { SUPPORTED_LANGUAGES } from "./translations/TranslationTemplate";

export class ClientBootstrap extends AbstractBootstrap {
    private static readonly CURRENT_LANGUAGE_KEY = "CURRENT_LANGUAGE_KEY";

    @Inject() viewHandler: AbstractViewHandler;

    async boot(): Promise<void> {
        //-- get default language from local storage
        const lang =
            localStorage.getItem(ClientBootstrap.CURRENT_LANGUAGE_KEY) ||
            SUPPORTED_LANGUAGES[0];
        this.viewHandler.switchLanguage(lang);
    }
}
