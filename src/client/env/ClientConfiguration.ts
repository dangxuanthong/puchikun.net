import { AbstractConfiguration, Required, DTO, LogLevel } from "@clairejs/core";

@DTO({ strict: true })
export class ClientConfiguration extends AbstractConfiguration {
    @Required()
    LOG_LEVEL: LogLevel;

    @Required()
    API_SERVER_URL: string;

    @Required()
    GOOGLE_CLIENT_ID: string;
}
