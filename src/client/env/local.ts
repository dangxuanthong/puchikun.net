import { ClientConfiguration } from "./ClientConfiguration";
import { LogLevel } from "@clairejs/core";
const env: ClientConfiguration = {
    LOG_LEVEL: LogLevel.DEBUG,
    API_SERVER_URL: "http://localhost:1998",
    GOOGLE_CLIENT_ID:
        "175710315121-q2abbdlklhosqdr7cto7pqsu0hnfmueu.apps.googleusercontent.com"
};

export default env;
