import { ClientConfiguration } from "./ClientConfiguration";
import { LogLevel } from "@clairejs/core";
const env: ClientConfiguration = {
    LOG_LEVEL: LogLevel.ERROR,
    API_SERVER_URL: "https://api.puchikun.net",
    GOOGLE_CLIENT_ID:
        "175710315121-q2abbdlklhosqdr7cto7pqsu0hnfmueu.apps.googleusercontent.com"
};

export default env;
