import {
    DefaultLogger,
    ConsoleLogMedium,
    ConfigurationResolver
} from "@clairejs/core";
import {
    ClaireClientBuilder,
    VueHandler,
    AxiosApiProvider
} from "@clairejs/client";

import Antd from "ant-design-vue";
import "ant-design-vue/dist/antd.css";

import PerfectScrollbar from "vue2-perfect-scrollbar";
import "vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css";

import { ClientConfiguration } from "./env/ClientConfiguration";
import {
    TranslationTemplate,
    SUPPORTED_LANGUAGES
} from "./translations/TranslationTemplate";
import { ClientBootstrap } from "./bootstrap";

import App from "./pages/App.vue";
import "./styles/root.scss";

import "./routers/HomeRouter";
import "./routers/PersonalRouter";
import { ErrorHandler } from "./middleware/ErrorHandler";

const env = process.env.NODE_ENV || "local";

const configResolver = ConfigurationResolver.resolve(
    ClientConfiguration,
    import(`./env/${env}`)
);

const client = new ClaireClientBuilder<ClientConfiguration>()
    .setConfiguration(env, configResolver)
    .setLogger(
        config => new DefaultLogger(config.LOG_LEVEL, [new ConsoleLogMedium()])
    )
    .setTranslations(TranslationTemplate, SUPPORTED_LANGUAGES, code =>
        import(`./translations/${code}`)
    )
    .setViewHandler(
        () =>
            new VueHandler({
                rootElementSelector: "#app",
                rootComponentClass: App,
                plugins: [Antd, PerfectScrollbar],
                errorHandler: new ErrorHandler()
            })
    )
    .setApiProvider(config => new AxiosApiProvider(config.API_SERVER_URL))
    .setBootstrap(() => new ClientBootstrap())
    .build();

client.boot().catch((err: any) => {
    console.log(err);
});
