import { AbstractErrorHandler } from "@clairejs/client";
import { Inject, AbstractLogger } from "@clairejs/core";

export class ErrorHandler extends AbstractErrorHandler {
    @Inject() logger: AbstractLogger;
    handle(err: any, component?: any): void {
        if (component) {
            (component as Vue).$message.error(
                err.name && err.extraInfo
                    ? `${err.name}: ${err.extraInfo}`
                    : err.message
            );
        } else {
            this.logger.error(err);
        }
    }
}
