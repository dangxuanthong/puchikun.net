import { AbstractModel } from "@clairejs/core";

import { VuexCrudModule } from "@clairejs/client";

export abstract class CrudModule<
    T extends AbstractModel
> extends VuexCrudModule<T> {}
