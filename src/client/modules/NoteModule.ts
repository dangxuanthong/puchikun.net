import { GetManyResponseBody, Inject, OrderDirection } from "@clairejs/core";
import { VueModule, Action } from "@clairejs/client";
import {
    CreateNoteRequestBody,
    CreateNoteResponseBody
} from "../../common/dtos/note";
import { Note } from "../../common/models/Note";
import { CrudModule } from "./CrudModule";
import { SearchNoteQueries, SearchMyNoteQueries } from "../../common/dtos/note";
import { NoteTagModule } from "./NoteTagModule";

@VueModule()
export class NoteModule extends CrudModule<Note> {
    @Inject() noteTagModule: NoteTagModule;

    public constructor(options: any) {
        super(Note, options);
    }

    @Action
    public async createNote(payload: CreateNoteRequestBody): Promise<Note> {
        const result = await this.apiProvider.post<CreateNoteResponseBody>(
            "/note",
            payload
        );
        return result.record;
    }

    @Action
    public async updateNote(data: {
        oldNote: Partial<Note> & Partial<CreateNoteRequestBody>;
        newNote: Partial<CreateNoteRequestBody>;
    }): Promise<void> {
        if (this.getDiff(data.oldNote, data.newNote)) {
            await this.apiProvider.put(
                `/note/${data.oldNote.id}`,
                data.newNote
            );
        }
    }

    @Action async removeNote(note: Note) {
        await this.deleteMany({ fields: { id: [note.id] } });
        this.noteTagModule.decache(nt => nt.noteId === note.id);
    }

    @Action
    public async searchNote(queries: SearchNoteQueries): Promise<Note[]> {
        queries.order = [
            ...(queries.order || []),
            { isPublic: OrderDirection.ASC },
            { createdAt: OrderDirection.DESC }
        ];
        const result = await this.apiProvider.get<GetManyResponseBody<Note>>(
            `/note/search?${this.convertToQueryString(queries)}`
        );
        return result.records;
    }

    @Action
    public async searchMyNote(queries: SearchMyNoteQueries): Promise<Note[]> {
        queries.order = [
            ...(queries.order || []),
            { isPublic: OrderDirection.ASC },
            { createdAt: OrderDirection.DESC }
        ];
        const result = await this.apiProvider.get<GetManyResponseBody<Note>>(
            `/note/mine?${this.convertToQueryString(queries)}`
        );
        return result.records;
    }
}
