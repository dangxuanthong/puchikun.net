import { VueModule } from "@clairejs/client";
import { CrudModule } from "./CrudModule";
import { NoteTag } from "../../common/models/NoteTag";

@VueModule()
export class NoteTagModule extends CrudModule<NoteTag> {
    public constructor(options: any) {
        super(NoteTag, options);
    }
}
