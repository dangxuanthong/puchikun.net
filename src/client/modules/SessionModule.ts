import { Inject, AbstractLogger, Initable, IStartStop } from "@clairejs/core";
import {
    VuexModule,
    VueModule,
    AbstractApiProvider,
    Action,
    Mutation
} from "@clairejs/client";
import {
    CreateSessionRequestBody,
    CreateSessionResponseBody,
    SiteInfo
} from "../../common/dtos/session";

import { User } from "../../common/models/User";
import { LocalStorageKey } from "./constants";
import { NoteModule } from "./NoteModule";

@VueModule()
@Initable()
export class SessionModule extends VuexModule implements IStartStop {
    @Inject() private readonly noteModule: NoteModule;
    @Inject() private readonly apiProvider: AbstractApiProvider;

    user?: User = null!;

    async init(): Promise<void> {
        //-- try to read and parse info from local storage
        const payload = localStorage.getItem(LocalStorageKey.SESSION);
        if (payload) {
            try {
                const data = JSON.parse(payload) as CreateSessionResponseBody;
                if (data.accessToken.expiration > Date.now()) {
                    this.setUser(data.user);
                    this.apiProvider.setHeader(
                        "Authorization",
                        data.accessToken.token
                    );
                } else {
                    //-- remove expired token
                    localStorage.removeItem(LocalStorageKey.SESSION);
                }
            } catch (err) {
                //-- payload parse failed, ignore
            }
        }
    }

    exit(): number {
        return 0;
    }

    @Mutation
    setUser(user?: User) {
        this.user = user;
    }

    @Action
    logout() {
        this.apiProvider.setHeader("Authorization", "");
        this.noteModule.decache(n => true);
        localStorage.removeItem(LocalStorageKey.SESSION);
        this.setUser();
    }

    @Action
    async createSession(
        payload: CreateSessionRequestBody
    ): Promise<CreateSessionResponseBody> {
        const result = await this.apiProvider.post<CreateSessionResponseBody>(
            "/session",
            payload
        );
        this.apiProvider.setHeader("Authorization", result.accessToken.token);
        localStorage.setItem(LocalStorageKey.SESSION, JSON.stringify(result));
        this.setUser(result.user);
        return result;
    }

    @Action
    async fetchSiteInfo(): Promise<SiteInfo> {
        return this.apiProvider.get<SiteInfo>("/site");
    }
}
