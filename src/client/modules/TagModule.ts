import { Inject, GetManyQueries } from "@clairejs/core";
import { VueModule, Action } from "@clairejs/client";
import { CrudModule } from "./CrudModule";
import { Tag } from "../../common/models/Tag";
import { Settings } from "../../common/constants";
import { NoteTagModule } from "./NoteTagModule";
import { SessionModule } from "./SessionModule";
import { LocalStorageKey } from "./constants";

@VueModule()
export class TagModule extends CrudModule<Tag> {
    @Inject() noteTagModule: NoteTagModule;
    @Inject() sessionModule: SessionModule;

    public constructor(options: any) {
        super(Tag, options);
    }

    public getRecentTagsFromLocalStorage(): Partial<Tag>[] {
        const data = localStorage.getItem(LocalStorageKey.RECENT_TAGS);
        return data ? (JSON.parse(data) as Tag[]) : [];
    }

    public saveTagsToLocalStorage(tags: Partial<Tag>[]) {
        //-- save to local storage
        const savedTags = this.getRecentTagsFromLocalStorage();
        for (const tag of tags) {
            if (!savedTags.find(t => t.tag === tag.tag)) {
                savedTags.unshift(tag);
            }
        }
        localStorage.setItem(
            LocalStorageKey.RECENT_TAGS,
            JSON.stringify(savedTags.slice(0, Settings.MAX_TAGS_PER_NOTE))
        );
    }

    public getTagsOfNote(id: number): Tag[] {
        const tagIds = this.noteTagModule.instances
            .filter(i => i.noteId === id)
            .map(i => i.tagId);
        return this.instances.filter(i => tagIds.includes(i.id));
    }

    @Action
    fetchMany(queries: GetManyQueries<Tag>) {
        queries.limit = Settings.MAX_TAGS_PER_NOTE;
        return super.fetchMany(queries);
    }
}
