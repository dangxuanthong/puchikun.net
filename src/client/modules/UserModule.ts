import { VueModule } from "@clairejs/client";
import { User } from "../../common/models/User";
import { CrudModule } from "./CrudModule";

@VueModule()
export class UserModule extends CrudModule<User> {
    constructor(options: any) {
        super(User, options);
    }
}
