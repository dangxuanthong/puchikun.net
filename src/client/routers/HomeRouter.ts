import { AbstractRouter, ViewRoute, Router } from "@clairejs/client";
import HomePage from "../pages/HomePage.vue";
import AboutPage from "../pages/AboutPage.vue";
import SearchResultPage from "../pages/SearchResultPage.vue";
import NoteViewPage from "../pages/NoteViewPage.vue";
import NotFoundPage from "../pages/NotFoundPage.vue";

@Router()
export class HomeRouter extends AbstractRouter {
    getRoutes(): ViewRoute {
        return {
            "": HomePage,
            "/about": AboutPage,
            "/search": SearchResultPage,
            "/note/:id": NoteViewPage,
            "/404": NotFoundPage
        };
    }
}
