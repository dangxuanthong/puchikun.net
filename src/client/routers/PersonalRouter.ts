import { Inject } from "@clairejs/core";
import {
    AbstractRouter,
    ViewRoute,
    Router,
    AbstractViewMiddleware,
    UrlInfo
} from "@clairejs/client";
import MyNotesPage from "../pages/MyNotesPage.vue";
import { SessionModule } from "../modules/SessionModule";

class AuthMiddleware extends AbstractViewMiddleware {
    @Inject() sessionModule: SessionModule;

    async intercept(from: UrlInfo, to: UrlInfo): Promise<void> {
        if (!this.sessionModule.user) {
            throw 404;
        }
    }
}

@Router()
export class PersonalRouter extends AbstractRouter {
    getMiddleware() {
        return [new AuthMiddleware()];
    }
    getRoutes(): ViewRoute {
        return {
            "/mynotes": MyNotesPage
        };
    }
}
