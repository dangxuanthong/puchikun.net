import { AbstractTranslation } from "@clairejs/client";
import { Injectable } from "@clairejs/core";

export const SUPPORTED_LANGUAGES = ["vi"];

@Injectable()
export class TranslationTemplate extends AbstractTranslation {
    test: {
        test1: any;
        test2: any;
    };
    note_creation_success: any;
    note_update_success: any;
    click_for_detail: any;
    remove_note_confirm: any;
}
