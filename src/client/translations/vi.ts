import { TranslationTemplate } from "./TranslationTemplate";
const translation: TranslationTemplate = {
    test: {
        test1: "Tao test",
        test2: "Mi test"
    },
    note_creation_success: "Tạo ghi chú thành công",
    note_update_success: "Cập nhật ghi chú thành công",
    click_for_detail: "Nhấn để xem chi tiết",
    remove_note_confirm: "Xóa ghi chú này?"
};

export default translation;
