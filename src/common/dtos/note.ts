import { DTO, Required, Vector, Enum, GetManyQueries } from "@clairejs/core";
import { SearchOperator } from "../enums/SearchOperator";
import { Note } from "../models/Note";
import { Settings } from "../constants";

@DTO({ strict: true })
export class CreateNoteRequestBody {
    @Required()
    content: string;

    @Required()
    isPublic: boolean;

    @Vector({ minLength: 0, maxLength: Settings.MAX_TAGS_PER_NOTE })
    @Required()
    tags: string[];
}

@DTO({ strict: true })
export class UpdateNoteRequestBody {
    @Required(false)
    content?: string;

    @Required(false)
    isPublic?: boolean;

    @Vector({ minLength: 0, maxLength: Settings.MAX_TAGS_PER_NOTE })
    @Required(false)
    tags?: string[];
}

export class CreateNoteResponseBody {
    record: Note;
}

export class UpdateNoteResponseBody {}

@DTO({ strict: false })
export class SearchNoteQueries extends GetManyQueries<Note> {
    @Required()
    @Enum({ enum: SearchOperator })
    op: SearchOperator;

    @Vector({ minLength: 1, maxLength: Settings.MAX_TAGS_PER_NOTE })
    tags: string[];
}

@DTO({ strict: false })
export class SearchMyNoteQueries extends GetManyQueries<Note> {
    @Required(false)
    @Enum({ enum: SearchOperator })
    op?: SearchOperator;

    @Required(false)
    @Vector({ minLength: 1 })
    tags?: string[];
}
