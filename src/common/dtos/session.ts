import { User } from "../models/User";
import { DTO, Required } from "@clairejs/core";

@DTO({ strict: true })
export class CreateSessionRequestBody {
    @Required()
    googleToken: string;
}

export class AccessToken {
    token: string;
    expiration: number;
}

export class CreateSessionResponseBody {
    accessToken: AccessToken;
    user: User;
    isNewUser: boolean;
}

export class AccessTokenPayload {
    userId: number;
}

export class SiteInfo {
    userCount: number;
    tagCount: number;
    noteCount: number;
}
