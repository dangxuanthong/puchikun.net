export enum SearchOperator {
    AND = "and",
    OR = "or"
}
