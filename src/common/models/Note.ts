import {
    AbstractModel,
    Model,
    TextField,
    FK,
    Field,
    Required,
    ServerValue, Http
} from "@clairejs/core";
import { User } from "./User";

@Model({ignoreCrud: [Http.POST, Http.PUT]})
export class Note extends AbstractModel {
    @Required()
    @TextField({ length: 8191 })
    content: string;

    @FK({ model: User })
    @ServerValue()
    ownerUserId: number;

    @Field()
    isPublic: boolean;
}
