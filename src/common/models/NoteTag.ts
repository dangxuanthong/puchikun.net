import { AbstractModel, Model, FK, Http } from "@clairejs/core";
import { Note } from "./Note";
import { Tag } from "./Tag";

@Model({ ignoreCrud: [Http.POST, Http.PUT, Http.DEL] })
export class NoteTag extends AbstractModel {
    @FK({ model: Note, cascade: true })
    noteId: number;

    @FK({ model: Tag, cascade: true })
    tagId: number;
}
