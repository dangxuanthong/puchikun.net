import { AbstractModel, Model, Unique, TextField } from "@clairejs/core";

@Model()
export class SystemSetting extends AbstractModel {
    @Unique()
    key: string;

    @TextField({ length: 2048 })
    value: string;
}
