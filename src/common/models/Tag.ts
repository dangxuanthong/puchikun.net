import {
    AbstractModel,
    Model,
    Unique,
    TextField,
    Http,
    Searchable
} from "@clairejs/core";

@Model({ ignoreCrud: [Http.POST, Http.PUT, Http.DEL] })
export class Tag extends AbstractModel {
    @Unique()
    @TextField({ length: 128 })
    @Searchable()
    tag: string;
}
