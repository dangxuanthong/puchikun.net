import { Http } from "@clairejs/core";
import {
    AbstractModel,
    Model,
    Unique,
    Field,
    URI,
    Mime,
    Required,
    NumberRange,
    Integral
} from "@clairejs/core";

@Model({ ignoreCrud: [Http.POST, Http.PUT, Http.DEL] })
export class User extends AbstractModel {
    @Field()
    name: string;

    @Field()
    isSuperUser: boolean;

    @Unique()
    googleEmail: string;

    @URI({ type: Mime.IMAGE })
    @Required(false)
    userAvatar?: string;

    @Required()
    @Integral()
    @NumberRange({ min: 0 })
    currentNoteNumber: number;

    @Required()
    @Integral()
    @NumberRange({ min: 0 })
    noteNumberLimit: number;
}
