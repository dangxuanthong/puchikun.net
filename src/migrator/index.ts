import path from "path";
import { DefaultSqlDatabaseMigrator, SqlProvider } from "@clairejs/server";
import { configResolver } from "../server/app";

import "../common/models/User";
import "../common/models/Tag";
import "../common/models/Note";
import "../common/models/NoteTag";
import "../common/models/SystemSetting";

(async () => {
    const config = await configResolver;

    const databaseMigrator = new DefaultSqlDatabaseMigrator(
        SqlProvider.POSTGRES,
        config.DB_CONNECTION_STRING!,
        path.join(__dirname, "./migration")
    );

    await databaseMigrator.migrate();
    process.exit(0);
})().catch(err => {
    console.log(err);
    process.exit(1);
});
