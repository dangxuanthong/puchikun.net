"use strict";
module.exports = {
    up: async (queryInterface, Sequelize) => {
        const t = await queryInterface.sequelize.transaction({
            autoCommit: false
        });
        try {
            await queryInterface.createTable(
                "user",
                {
                    id: {
                        primaryKey: true,
                        autoIncrement: true,
                        allowNull: false,
                        type: Sequelize.INTEGER
                    },
                    createdAt: { allowNull: false, type: Sequelize.BIGINT },
                    name: { allowNull: false, type: Sequelize.STRING },
                    isSuperUser: { allowNull: false, type: Sequelize.BOOLEAN },
                    googleEmail: { allowNull: false, type: Sequelize.STRING },
                    userAvatar: { allowNull: true, type: Sequelize.STRING },
                    currentNoteNumber: {
                        allowNull: false,
                        type: Sequelize.INTEGER
                    },
                    noteNumberLimit: {
                        allowNull: false,
                        type: Sequelize.INTEGER
                    }
                },
                { charset: "utf8", transaction: t }
            );
            await queryInterface.createTable(
                "tag",
                {
                    id: {
                        primaryKey: true,
                        autoIncrement: true,
                        allowNull: false,
                        type: Sequelize.INTEGER
                    },
                    createdAt: { allowNull: false, type: Sequelize.BIGINT },
                    tag: { allowNull: false, type: Sequelize.STRING(128) }
                },
                { charset: "utf8", transaction: t }
            );
            await queryInterface.createTable(
                "note",
                {
                    id: {
                        primaryKey: true,
                        autoIncrement: true,
                        allowNull: false,
                        type: Sequelize.INTEGER
                    },
                    createdAt: { allowNull: false, type: Sequelize.BIGINT },
                    content: { allowNull: false, type: Sequelize.STRING(2048) },
                    ownerUserId: { allowNull: false, type: Sequelize.INTEGER },
                    isPublic: { allowNull: false, type: Sequelize.BOOLEAN }
                },
                { charset: "utf8", transaction: t }
            );
            await queryInterface.createTable(
                "notetag",
                {
                    id: {
                        primaryKey: true,
                        autoIncrement: true,
                        allowNull: false,
                        type: Sequelize.INTEGER
                    },
                    createdAt: { allowNull: false, type: Sequelize.BIGINT },
                    noteId: { allowNull: false, type: Sequelize.INTEGER },
                    tagId: { allowNull: false, type: Sequelize.INTEGER }
                },
                { charset: "utf8", transaction: t }
            );
            await queryInterface.createTable(
                "systemsetting",
                {
                    id: {
                        primaryKey: true,
                        autoIncrement: true,
                        allowNull: false,
                        type: Sequelize.INTEGER
                    },
                    createdAt: { allowNull: false, type: Sequelize.BIGINT },
                    key: { allowNull: false, type: Sequelize.STRING },
                    value: { allowNull: false, type: Sequelize.STRING(2048) }
                },
                { charset: "utf8", transaction: t }
            );
            await queryInterface.addConstraint("user", {
                fields: ["googleEmail"],
                type: "unique",
                name: "user_googleEmail_un",
                transaction: t
            });
            await queryInterface.addConstraint("tag", {
                fields: ["tag"],
                type: "unique",
                name: "tag_tag_un",
                transaction: t
            });
            await queryInterface.addConstraint("note", {
                fields: ["ownerUserId"],
                type: "foreign key",
                name: "note_ownerUserId_fk",
                references: { table: "user", field: "id" },
                transaction: t
            });
            await queryInterface.addConstraint("notetag", {
                fields: ["noteId"],
                type: "foreign key",
                name: "notetag_noteId_fk",
                references: { table: "note", field: "id" },
                onDelete: "CASCADE",
                onUpdate: "CASCADE",
                transaction: t
            });
            await queryInterface.addConstraint("notetag", {
                fields: ["tagId"],
                type: "foreign key",
                name: "notetag_tagId_fk",
                references: { table: "tag", field: "id" },
                onDelete: "CASCADE",
                onUpdate: "CASCADE",
                transaction: t
            });
            await queryInterface.addConstraint("systemsetting", {
                fields: ["key"],
                type: "unique",
                name: "systemsetting_key_un",
                transaction: t
            });
            await t.commit();
        } catch (err) {
            await t.rollback();
            throw err;
        }
    },

    down: async (queryInterface, Sequelize) => {
        const t = await queryInterface.sequelize.transaction({
            autoCommit: false
        });
        try {
            await queryInterface.removeConstraint(
                "systemsetting",
                "systemsetting_key_un",
                { transaction: t }
            );
            await queryInterface.removeConstraint(
                "notetag",
                "notetag_tagId_fk",
                { transaction: t }
            );
            await queryInterface.removeConstraint(
                "notetag",
                "notetag_noteId_fk",
                { transaction: t }
            );
            await queryInterface.removeConstraint(
                "note",
                "note_ownerUserId_fk",
                { transaction: t }
            );
            await queryInterface.removeConstraint("tag", "tag_tag_un", {
                transaction: t
            });
            await queryInterface.removeConstraint(
                "user",
                "user_googleEmail_un",
                { transaction: t }
            );
            await queryInterface.dropTable("systemsetting", { transaction: t });
            await queryInterface.dropTable("notetag", { transaction: t });
            await queryInterface.dropTable("note", { transaction: t });
            await queryInterface.dropTable("tag", { transaction: t });
            await queryInterface.dropTable("user", { transaction: t });
            await t.commit();
        } catch (err) {
            await t.rollback();
            throw err;
        }
    }
};
