'use strict';
module.exports = {
    up: async (queryInterface, Sequelize) => {
        const t = await queryInterface.sequelize.transaction({autoCommit: false});
        try {
            await queryInterface.changeColumn("note", "content", {
                allowNull: false,
                type: Sequelize.STRING(4095),
            }, {"transaction": t});
            await t.commit();
        } catch (err) {
            await t.rollback();
            throw(err);
        }
    },

    down: async (queryInterface, Sequelize) => {
        const t = await queryInterface.sequelize.transaction({autoCommit: false});
        try {
            await queryInterface.changeColumn("note", "content", {
                allowNull: false,
                type: Sequelize.STRING(2048),
            }, {"transaction": t});
            await t.commit();
        } catch (err) {
            await t.rollback();
            throw(err);
        }
    }
};