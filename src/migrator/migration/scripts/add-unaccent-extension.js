"use strict";
module.exports = {
    up: async (queryInterface, Sequelize) => {
        const t = await queryInterface.sequelize.transaction({
            autoCommit: false
        });
        try {
            await queryInterface.sequelize.query("CREATE EXTENSION unaccent");
            await t.commit();
        } catch (err) {
            await t.rollback();
            throw err;
        }
    },

    down: async (queryInterface, Sequelize) => {
        const t = await queryInterface.sequelize.transaction({
            autoCommit: false
        });
        try {
            await queryInterface.sequelize.query("DROP EXTENSION unaccent");
            await t.commit();
        } catch (err) {
            await t.rollback();
            throw err;
        }
    }
};
