import {
    DefaultLogger,
    ConsoleLogMedium,
    ConfigurationResolver
} from "@clairejs/core";

import {
    ClaireServerBuilder,
    DefaultHttpRequestHandler,
    DefaultSqlDatabaseAdapter,
    SqlProvider
} from "@clairejs/server";
import { ServerConfiguration } from "./env/ServerConfiguration";
import { ServerBootstrap } from "./bootstrap";
import { TokenParser } from "./middleware/TokenParser";
import { AuthorizationProvider } from "./middleware/AuthorizationProvider";

import "./controllers/SessionController";
import "./controllers/NoteController";
import "./controllers/TagController";
import "./controllers/UserController";
import "./controllers/NoteTagController";

export const env = process.env.NODE_ENV || "local";

export const configResolver = ConfigurationResolver.resolve(
    ServerConfiguration,
    import(`./env/${env}`)
);

export const server = new ClaireServerBuilder<ServerConfiguration>()
    .setConfiguration(env, configResolver)
    .setLogger(
        config => new DefaultLogger(config.LOG_LEVEL, [new ConsoleLogMedium()])
    )
    .setHttpRequestHandler(
        () =>
            new DefaultHttpRequestHandler({
                authorizationProvider: new AuthorizationProvider(),
                middleware: [new TokenParser()]
            })
    )
    .setDatabaseAdapter(
        config =>
            new DefaultSqlDatabaseAdapter(
                SqlProvider.POSTGRES,
                config.DB_CONNECTION_STRING!
            )
    )
    .setBootstrap(() => new ServerBootstrap())
    .build();
