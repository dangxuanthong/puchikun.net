import { Inject, AbstractBootstrap } from "@clairejs/core";
import { AbstractDatabaseAdapter, Op } from "@clairejs/server";
import { ServerConfiguration } from "./env/ServerConfiguration";
import { User } from "../common/models/User";

export class ServerBootstrap extends AbstractBootstrap {
    @Inject() config: ServerConfiguration;
    @Inject() databaseAdapter: AbstractDatabaseAdapter;

    async boot(): Promise<void> {
        //-- create default first admin user
        let user = await this.databaseAdapter.use(User).getOne({
            [Op.EQ]: { googleEmail: this.config.DEFAULT_ROOT_USER_EMAIL }
        });
        if (!user) {
            user = new User();
            user.noteNumberLimit = -1;
            user.currentNoteNumber = 0;
            user.name = "Immort";
            user.isSuperUser = true;
            user.googleEmail = this.config.DEFAULT_ROOT_USER_EMAIL!;
            await this.databaseAdapter.use(User).createOne(user);
        }
    }
}
