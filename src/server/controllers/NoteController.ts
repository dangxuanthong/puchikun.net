import {
    Http,
    Inject,
    GetManyQueries,
    OrderDirection,
    UpdateManyQueries,
    ClaireError,
    AbstractLogger,
    GetManyResponseBody, IdParam
} from "@clairejs/core";
import {
    CrudHttpController,
    Endpoint,
    HttpRequest,
    Controller,
    Validator,
    Transactional,
    TransactionLink,
    Op,
    AbstractDatabaseAdapter,
    OpenAccess
} from "@clairejs/server";
import { Note } from "../../common/models/Note";
import {
    CreateNoteRequestBody,
    CreateNoteResponseBody
} from "../../common/dtos/note";
import { User } from "../../common/models/User";
import { Tag } from "../../common/models/Tag";
import { NoteTag } from "../../common/models/NoteTag";
import {
    SearchNoteQueries,
    SearchMyNoteQueries,
    UpdateNoteRequestBody,
    UpdateNoteResponseBody
} from "../../common/dtos/note";
import { SearchOperator } from "../../common/enums/SearchOperator";
import { Errors } from "../../common/constants";

@Controller()
export class NoteController extends CrudHttpController<Note> {
    @Inject() databaseAdapter: AbstractDatabaseAdapter;
    @Inject() logger: AbstractLogger;

    constructor() {
        super(Note);
    }

    private convertLimitOptionToQuery(limit?: number, page?: number): string {
        return limit
            ? `LIMIT ${limit} ${page ? `OFFSET ${(page - 1) * limit}` : ""}`
            : "";
    }

    private convertOrderOptionToQuery(
        orderOption?: {
            [k in keyof Note]?: OrderDirection;
        }[]
    ): string {
        return orderOption
            ? `ORDER BY ${orderOption.reduce(
                  (collector, fieldOrder, index) =>
                      `${collector} n."${Object.keys(fieldOrder)[0]}" ${
                          (fieldOrder as any)[Object.keys(fieldOrder)[0]]
                      }${index < orderOption!.length - 1 ? "," : ""}`,
                  ""
              )}`
            : "";
    }

    @Transactional(TransactionLink.INHERIT_OR_THROW)
    private async updateTagsOfNote(
        note: Note,
        oldTagIds: number[],
        newTags: string[]
    ) {
        const tx = await this.getCurrentTransaction();
        const newTagInstances = newTags.length
            ? (
                  await tx
                      .use(Tag)
                      .getMany(
                          { [Op.IN]: { tag: newTags } },
                          { projection: ["id", "tag"] }
                      )
              ).records
            : [];

        const newTagIds = newTagInstances.map(r => r.id!);

        const toRemoveIds = oldTagIds.filter(id => !newTagIds.includes(id));
        if (toRemoveIds.length) {
            //-- remove note tag
            await tx.use(NoteTag).deleteMany({
                [Op.AND]: [
                    { [Op.EQ]: { noteId: note.id } },
                    { [Op.IN]: { tagId: toRemoveIds } }
                ]
            });
        }

        const toAddTags = newTags.filter(
            tag => !newTagInstances.find(t => t.tag === tag)
        );

        const toAddIds = newTagIds.filter(id => !oldTagIds.includes(id));

        if (toAddTags.length) {
            const newTags = await tx
                .use(Tag)
                .createMany(toAddTags.map(t => ({ tag: t })));
            toAddIds.push(...newTags.map(t => t.id));
        }

        //-- add note tag
        await tx
            .use(NoteTag)
            .createMany(toAddIds.map(id => ({ noteId: note.id, tagId: id })));
    }

    @Endpoint({ method: Http.POST, url: "/note" })
    @Validator({ body: CreateNoteRequestBody })
    @Transactional(TransactionLink.CREATE_NEW)
    async createNote(request: HttpRequest): Promise<CreateNoteResponseBody> {
        const tx = await this.getCurrentTransaction();
        const user = request.getValue<User>("user");
        const body = request.getBody<CreateNoteRequestBody>()!;

        if (
            user.noteNumberLimit >= 0 &&
            user.noteNumberLimit <= user.currentNoteNumber
        ) {
            throw new ClaireError(
                Errors.QUOTA_EXCEEDED,
                `Note limit reached ${user.noteNumberLimit}`
            );
        }

        //-- create note
        let note = new Note();
        note.content = body.content;
        note.isPublic = body.isPublic;
        note.ownerUserId = user.id;
        note = await tx.use(Note).createOne(note);

        //-- check and create tag
        if (body.tags.length) {
            await this.updateTagsOfNote(note, [], body.tags);
        }

        user.currentNoteNumber = user.currentNoteNumber + 1;
        await tx.use(User).updateOne(user);

        return { record: note };
    }

    @Endpoint({ method: Http.PUT, url: "/note/:id" })
    @Transactional(TransactionLink.CREATE_NEW)
    @Validator({ body: UpdateNoteRequestBody, params: IdParam })
    async updateNote(request: HttpRequest): Promise<UpdateNoteResponseBody> {
        const tx = await this.getCurrentTransaction();
        const user = request.getValue<User>("user");
        const params = request.getParams<IdParam>();
        const body = request.getBody<UpdateNoteRequestBody>()!;

        //-- update note
        const noteId = params.id;
        const note = await tx.use(Note).getOne({ [Op.EQ]: { id: noteId } });

        if (!note) {
            throw new ClaireError(
                Errors.RESOURCE_NOT_FOUND,
                `Cannot find note with id: ${noteId}`
            );
        }

        if (user.id !== note.ownerUserId) {
            throw new ClaireError(
                Errors.OWNERSHIP_REQUIRED,
                `Only creators can edit their notes`
            );
        }

        if (body.content !== undefined) {
            note.content = body.content;
        }

        if (body.isPublic !== undefined) {
            note.isPublic = body.isPublic;
        }

        await tx.use(Note).updateOne(note);

        //-- check and udpate tag
        if (body.tags) {
            const oldNoteTags = await tx
                .use(NoteTag)
                .getMany({ [Op.EQ]: { noteId } });

            await this.updateTagsOfNote(
                note,
                oldNoteTags.records.map(nt => nt.tagId!),
                body.tags
            );
        }

        return {};
    }

    @OpenAccess()
    @Validator({ query: SearchNoteQueries })
    @Endpoint({ method: Http.GET, url: "/note/search" })
    async searchNote(request: HttpRequest) {
        const user = request.getValue<User | undefined>("user");

        const queries = request.getQuery<
            SearchNoteQueries & GetManyQueries<Note>
        >();

        const tags = await this.databaseAdapter.use(Tag).getMany(
            {
                [Op.IN]: { tag: queries.tags }
            },
            { projection: ["id"] }
        );

        const tagIds: string = tags.records
            .map(t => t.id)
            .reduce(
                (collector, id, index) =>
                    `${collector}${id}${
                        index < tags.records.length - 1 ? "," : ""
                    }`,
                ""
            );

        const sqlParts = [
            "SELECT",
            queries.projection
                ? queries.projection.reduce(
                      (collector, fieldName, index) =>
                          `${collector}n."${fieldName}"${
                              index < queries.projection!.length - 1 ? ", " : ""
                          }`,
                      ""
                  )
                : "*", //-- select all if no projection
            `FROM note n WHERE`,
            `(n."isPublic" = TRUE ${
                user ? `OR n."ownerUserId" = ${user.id}` : ""
            })`,
            `AND n."id" IN (SELECT nt."noteId" FROM NoteTag nt
                                WHERE nt."tagId" IN`,
            `(${tagIds})`, // bracket array syntax of tag ids
            `GROUP BY nt."noteId" HAVING array_agg(nt."tagId")`,
            queries.op === SearchOperator.AND ? "@>" : "&&", // NOTE: progres syntax use @> for 'array contains', && for 'array intersect'
            `ARRAY[${tagIds}])`, // NOTE: postgres array syntax of array
            this.convertOrderOptionToQuery(queries.order),
            this.convertLimitOptionToQuery(queries.limit, queries.page)
        ];

        const sqlQuery =
            sqlParts
                .reduce((collector, part) => collector + ` ${part} `, "")
                .replace(/\n/g, "") + ";";

        this.logger.debug(sqlQuery);

        const notes: Note[] = (
            await this.databaseAdapter.use(Note).rawQuery(sqlQuery)
        )[0];

        //-- we return total as -1 to disable finite pagination
        return {
            total: -1,
            records: notes.map(n => ({
                ...n,
                createdAt: Number(n.createdAt)
            }))
        };
    }

    @Validator({ query: SearchMyNoteQueries })
    @Endpoint({ method: Http.GET, url: "/note/mine" })
    async searchMyNote(request: HttpRequest) {
        const user = request.getValue<User>("user");

        const queries = request.getQuery<SearchMyNoteQueries>();

        const tags =
            queries.tags &&
            (await this.databaseAdapter.use(Tag).getMany(
                {
                    [Op.IN]: { tag: queries.tags }
                },
                { projection: ["id"] }
            ));

        const tagIds =
            tags &&
            tags.records
                .map(t => t.id)
                .reduce(
                    (collector, id, index) =>
                        `${collector}${id}${
                            index < tags.records.length - 1 ? "," : ""
                        }`,
                    ""
                );

        const sqlParts = [
            "SELECT",
            queries.projection
                ? queries.projection.reduce(
                      (collector, fieldName, index) =>
                          `${collector}n."${fieldName}"${
                              index < queries.projection!.length - 1 ? ", " : ""
                          }`,
                      ""
                  )
                : "*", //-- select all if no projection
            `FROM note n WHERE n."ownerUserId" = ${user.id}`,
            tags
                ? `AND n."id" IN (SELECT nt."noteId" FROM NoteTag nt
                                WHERE nt."tagId" IN (${tagIds}) 
                                GROUP BY nt."noteId" HAVING array_agg(nt."tagId") ${
                                    queries.op === SearchOperator.AND
                                        ? "@>"
                                        : "&&"
                                } ARRAY[${tagIds}])`
                : "",
            this.convertOrderOptionToQuery(queries.order),
            this.convertLimitOptionToQuery(queries.limit, queries.page)
        ];

        const sqlQuery =
            sqlParts
                .reduce((collector, part) => collector + ` ${part} `, "")
                .replace(/\n/g, "") + ";";

        this.logger.debug(sqlQuery);

        const notes: Note[] = (
            await this.databaseAdapter.use(Note).rawQuery(sqlQuery)
        )[0];

        //-- we return total as -1 to disable finite pagination
        return {
            total: -1,
            records: notes.map(n => ({
                ...n,
                createdAt: Number(n.createdAt)
            }))
        };
    }

    @Transactional(TransactionLink.CREATE_NEW)
    async deleteMany(request: HttpRequest) {
        const user = request.getValue<User>("user");
        const notes = await this.getMany(request);

        if (!notes.records.length) {
            return { modified: [] };
        }

        if (
            !user.isSuperUser &&
            notes.records.find(r => r.ownerUserId !== user.id)
        ) {
            throw new ClaireError(
                Errors.OWNERSHIP_REQUIRED,
                `Some of notes require author permission`
            );
        }

        return super.deleteMany(request);
    }

    @OpenAccess()
    @Transactional(TransactionLink.CREATE_NEW)
    async getMany(request: HttpRequest): Promise<GetManyResponseBody<Note>> {
        const result = await super.getMany(request);
        //-- check user
        const user = request.getValue<User | undefined>("user");

        //-- filter result
        result.records = result.records.filter(
            r => r.isPublic || r.ownerUserId === user?.id
        );
        return result;
    }
}
