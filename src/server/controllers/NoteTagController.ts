import { CrudHttpController, Controller } from "@clairejs/server";
import { NoteTag } from "../../common/models/NoteTag";

@Controller()
export class NoteTagController extends CrudHttpController<NoteTag> {
    public constructor() {
        super(NoteTag);
    }
}
