import {
    Http,
    Initable,
    IStartStop,
    Inject,
    AbstractLogger,
    ClaireError
} from "@clairejs/core";

import {
    AbstractHttpController,
    Controller,
    HttpRequest,
    Endpoint,
    Validator,
    AbstractDatabaseAdapter,
    Op,
    OpenAccess
} from "@clairejs/server";

import { OAuth2Client } from "google-auth-library";
import { ServerConfiguration } from "../env/ServerConfiguration";
import { User } from "../../common/models/User";
import { Errors, Settings } from "../../common/constants";
import { SessionService } from "../services/SessionService";
import { SiteInfo } from "../../common/dtos/session";
import {
    CreateSessionRequestBody,
    CreateSessionResponseBody
} from "../../common/dtos/session";
import { Tag } from "../../common/models/Tag";
import { Note } from "../../common/models/Note";

@Controller()
@Initable()
export class SessionController extends AbstractHttpController
    implements IStartStop {
    @Inject() config: ServerConfiguration;
    @Inject() logger: AbstractLogger;
    @Inject() databaseAdapter: AbstractDatabaseAdapter;
    @Inject() sessionService: SessionService;
    private googleClient: OAuth2Client;

    async init(): Promise<void> {
        this.googleClient = new OAuth2Client(this.config.GOOGLE_CLIENT_ID);
    }

    exit(): number {
        return 0;
    }

    @OpenAccess()
    @Endpoint({ method: Http.GET, url: "/site" })
    async getSiteInfo(): Promise<SiteInfo> {
        const userCount = (
            await this.databaseAdapter
                .use(User)
                .rawQuery(`SELECT COUNT(*) as count FROM "user";`)
        )[0][0].count;

        const tagCount = (
            await this.databaseAdapter
                .use(Tag)
                .rawQuery(`SELECT COUNT(*) as count FROM "tag";`)
        )[0][0].count;

        const noteCount = (
            await this.databaseAdapter
                .use(Note)
                .rawQuery(`SELECT COUNT(*) as count FROM "note";`)
        )[0][0].count;

        return {
            userCount: Number(userCount),
            tagCount: Number(tagCount),
            noteCount: Number(noteCount)
        };
    }

    @OpenAccess()
    @Endpoint({ method: Http.POST, url: "/session" })
    @Validator({ body: CreateSessionRequestBody })
    async createSession(
        request: HttpRequest
    ): Promise<CreateSessionResponseBody> {
        const body = request.getBody<CreateSessionRequestBody>()!;

        const ticket = await this.googleClient.verifyIdToken({
            idToken: body.googleToken,
            audience: this.config.GOOGLE_CLIENT_ID
        });
        const payload = ticket.getPayload();

        if (!payload || !payload.email) {
            throw new ClaireError(
                Errors.AUTHENTICATION_ERROR,
                "Invalid Google login token provided"
            );
        }

        //-- check to return user
        let isNewUser = false;
        let user = await this.databaseAdapter
            .use(User)
            .getOne({ [Op.EQ]: { googleEmail: payload.email } });

        if (!user) {
            //-- check to create user
            if (!payload.email_verified) {
                throw new ClaireError(
                    Errors.AUTHENTICATION_ERROR,
                    "Google email not verified"
                );
            }

            user = new User();
            user.noteNumberLimit = Settings.DEFAULT_MAX_NOTES_PER_USER;
            user.currentNoteNumber = 0;
            user.userAvatar = payload.picture;
            user.name = payload.name || payload.email;
            user.isSuperUser = false;
            user.googleEmail = payload.email;
            user = await this.databaseAdapter.use(User).createOne(user);

            isNewUser = true;
        }

        return {
            accessToken: await this.sessionService.createAccessToken({
                userId: user.id
            }),
            user,
            isNewUser: isNewUser
        };
    }
}
