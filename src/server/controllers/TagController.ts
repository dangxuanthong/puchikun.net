import {
    CrudHttpController,
    Controller,
    HttpRequest,
    OpenAccess
} from "@clairejs/server";
import { Tag } from "../../common/models/Tag";

@Controller()
export class TagController extends CrudHttpController<Tag> {
    constructor() {
        super(Tag);
    }

    @OpenAccess()
    getMany(request: HttpRequest) {
        return super.getMany(request);
    }
}
