import {
    CrudHttpController,
    Controller,
    HttpRequest,
    OpenAccess
} from "@clairejs/server";
import { User } from "../../common/models/User";

@Controller()
export class UserController extends CrudHttpController<User> {
    public constructor() {
        super(User);
    }

    @OpenAccess()
    getMany(request: HttpRequest) {
        return super.getMany(request);
    }
}
