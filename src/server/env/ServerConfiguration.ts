import { AbstractConfiguration, DTO, Required, LogLevel } from "@clairejs/core";

@DTO({ strict: false })
export class ServerConfiguration extends AbstractConfiguration {
    @Required()
    LOG_LEVEL: LogLevel;

    @Required()
    DEFAULT_ROOT_USER_EMAIL?: string;

    @Required()
    LISTEN_PORT: number;

    @Required()
    GOOGLE_CLIENT_ID: string;

    @Required()
    JWT_KEY?: string;

    @Required()
    DB_CONNECTION_STRING?: string;

    @Required()
    TOKEN_EXPIRATION_DURATION_S: number;
}
