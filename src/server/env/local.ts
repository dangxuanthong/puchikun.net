import { ServerConfiguration } from "./ServerConfiguration";
import { LogLevel } from "@clairejs/core";

const env: ServerConfiguration = {
    LOG_LEVEL: LogLevel.DEBUG,
    LISTEN_PORT: 1998,
    TOKEN_EXPIRATION_DURATION_S: 604800,
    GOOGLE_CLIENT_ID:
        "175710315121-q2abbdlklhosqdr7cto7pqsu0hnfmueu.apps.googleusercontent.com",
    DEFAULT_ROOT_USER_EMAIL: "dangxuanthong@gmail.com",
    DB_CONNECTION_STRING: "postgres:vinamilk@localhost:5432/puchikun-local-5",
    JWT_KEY: "97489748972489723894"
};

export default env;
