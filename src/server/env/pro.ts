import { ServerConfiguration } from "./ServerConfiguration";
import { LogLevel } from "@clairejs/core";

const env: ServerConfiguration = {
    LOG_LEVEL: LogLevel.ERROR,
    LISTEN_PORT: 2000,
    TOKEN_EXPIRATION_DURATION_S: 604800,
    GOOGLE_CLIENT_ID:
        "175710315121-q2abbdlklhosqdr7cto7pqsu0hnfmueu.apps.googleusercontent.com"
};

export default env;
