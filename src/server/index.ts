import { ExpressWrapper } from "@clairejs/server";

import { server } from "./app";

const port = 1998;
new ExpressWrapper(server)
    .listen(port)
    .then(() => {
        console.log(`Server is listening at port ${port}`);
    })
    .catch((err) => {
        console.log(err);
    });