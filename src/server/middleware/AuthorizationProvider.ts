import {
    AbstractHttpAuthorizationProvider,
    HttpRequest,
    MountedEndpointInfo
} from "@clairejs/server";
import { Inject, ClaireError } from "@clairejs/core";
import { SessionService } from "../services/SessionService";
import { User } from "../../common/models/User";
import { Errors } from "../../common/constants";

export class AuthorizationProvider extends AbstractHttpAuthorizationProvider {
    @Inject() sessionService: SessionService;

    async authorize(
        req: HttpRequest,
        _endpoint: MountedEndpointInfo
    ): Promise<void> {
        const user = req.getValue<User>("user");
        if (!user) {
            throw new ClaireError(
                Errors.AUTHENTICATION_ERROR,
                "Authentication required"
            );
        }
    }
}
