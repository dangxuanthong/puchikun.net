import { Inject, AbstractLogger } from "@clairejs/core";
import {
    AbstractHttpMiddleware,
    HttpRequest,
    AbstractDatabaseAdapter,
    Op
} from "@clairejs/server";
import { SessionService } from "../services/SessionService";
import { User } from "../../common/models/User";

export class TokenParser extends AbstractHttpMiddleware {
    @Inject() sessionService: SessionService;
    @Inject() databaseAdapter: AbstractDatabaseAdapter;
    @Inject() logger: AbstractLogger;

    async intercept(req: HttpRequest): Promise<void> {
        const token = req.getHeaders().authorization;

        this.logger.debug(`Processing ${req.getMethod()}:${req.getPathName()}`);
        if (token) {
            const accessTokenPayload = await this.sessionService.decodeAccessToken(
                token
            );
            if (accessTokenPayload) {
                const user = await this.databaseAdapter
                    .use(User)
                    .getOne({ [Op.EQ]: { id: accessTokenPayload.userId } });
                if (user) {
                    req.setValue("user", user);
                }
            }
        }
    }
}
