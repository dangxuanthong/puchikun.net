import { LambdaWrapper } from "@clairejs/server";
import { server } from "./app";

exports.handler = new LambdaWrapper(server, (event: any) => ({
    method: event.requestContext.http.method,
    rawPath: `${event.rawPath}?${event.rawQueryString}`,
    body: event.body && JSON.parse(event.body),
    headers: event.headers,
})).handler;
