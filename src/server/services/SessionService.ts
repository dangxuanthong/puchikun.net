import { Inject, Injectable } from "@clairejs/core";
import { AbstractService } from "@clairejs/server";
import jwt from "jsonwebtoken";

import { ServerConfiguration } from "../env/ServerConfiguration";
import { AccessTokenPayload, AccessToken } from "../../common/dtos/session";

@Injectable()
export class SessionService extends AbstractService {
    @Inject() private readonly config: ServerConfiguration;

    public async createAccessToken(
        payload: AccessTokenPayload
    ): Promise<AccessToken> {
        return {
            token: jwt.sign(payload, this.config.JWT_KEY!, {
                expiresIn: this.config.TOKEN_EXPIRATION_DURATION_S
            }),
            expiration:
                Date.now() + this.config.TOKEN_EXPIRATION_DURATION_S * 1000
        };
    }

    public async decodeAccessToken(
        token: string
    ): Promise<AccessTokenPayload | undefined> {
        try {
            return jwt.verify(
                token,
                this.config.JWT_KEY!
            ) as AccessTokenPayload;
        } catch (err) {
            return undefined;
        }
    }
}
