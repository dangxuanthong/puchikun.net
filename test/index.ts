import { handler } from "../src/server/serverless";

const event = {
    version: "2.0",
    routeKey: "$default",
    rawPath: "/",
    rawQueryString: "",
    headers: {
        accept:
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
        "accept-encoding": "gzip, deflate, br",
        "accept-language":
            "vi-VN,vi;q=0.9,fr-FR;q=0.8,fr;q=0.7,en-US;q=0.6,en;q=0.5",
        "content-length": "0",
        host: "gxxzlobuvk.execute-api.ap-southeast-1.amazonaws.com",
        referer:
            "https://ap-southeast-1.console.aws.amazon.com/apigateway/main/api-detail?api=gxxzlobuvk&integration=u6iad52&region=ap-southeast-1&routes=jlvwvle",
        "sec-fetch-dest": "document",
        "sec-fetch-mode": "navigate",
        "sec-fetch-site": "cross-site",
        "sec-fetch-user": "?1",
        "upgrade-insecure-requests": "1",
        "user-agent":
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36",
        "x-amzn-trace-id": "Root=1-5f51c01a-4f0bce46a713255adadbb554",
        "x-forwarded-for": "42.112.154.141",
        "x-forwarded-port": "443",
        "x-forwarded-proto": "https"
    },
    requestContext: {
        accountId: "637842621870",
        apiId: "gxxzlobuvk",
        domainName: "gxxzlobuvk.execute-api.ap-southeast-1.amazonaws.com",
        domainPrefix: "gxxzlobuvk",
        http: {
            method: "GET",
            path: "/",
            protocol: "HTTP/1.1",
            sourceIp: "42.112.154.141",
            userAgent:
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36"
        },
        requestId: "SUr0FiNLyQ0EMuA=",
        routeKey: "$default",
        stage: "$default",
        time: "04/Sep/2020:04:18:34 +0000",
        timeEpoch: 1599193114002
    },
    isBase64Encoded: false
};

handler(event)
    .then(response => console.log(response))
    .catch(err => console.log(err));
