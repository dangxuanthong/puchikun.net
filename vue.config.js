const path = require("path");
const fs = require("fs");
const webpack = require("webpack");

const packageJson = fs.readFileSync("./package.json");
const version = JSON.parse(packageJson).version || "";

module.exports = {
    pages: {
        index: {
            entry: "src/client/index.ts"
        }
    },
    outputDir: path.resolve(__dirname, "./client-dist/"),
    chainWebpack: config => {
        config.output.filename(`js/[name].[hash:8].js`);
        config.output.chunkFilename(`js/[name].[chunkhash:8].js`);
    },
    configureWebpack: {
        plugins: [
            new webpack.DefinePlugin({
                "process.env": {
                    PACKAGE_VERSION: '"' + version + '"'
                }
            })
        ]
    }
};
