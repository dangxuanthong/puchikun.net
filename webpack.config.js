const path = require("path");
const webpack = require("webpack");
const TerserPlugin = require("terser-webpack-plugin");

module.exports = {
    entry: "./src/server/serverless.ts",
    target: "node",
    module: {
        rules: [
            {
                test: /\.ts$/,
                loader: "ts-loader",
                options: {
                    configFile: "tsconfig-server.json"
                }
            },
            {
                //-- we don't use native pg package
                test: path.resolve(
                    __dirname,
                    "node_modules/pg/lib/native/index.js"
                ),
                use: "null-loader"
            }
        ]
    },
    plugins: [
        new webpack.ContextReplacementPlugin(
            /sequelize\/lib\/dialects\/abstract/,
            // express/lib/*
            path.resolve("node_modules"),
            {
                pg: "pg"
            }
        )
    ],
    resolve: {
        extensions: [".ts", ".js"]
    },
    output: {
        filename: "index.js",
        path: path.resolve(__dirname, "server-dist"),
        libraryTarget: "umd"
    },
    optimization: {
        nodeEnv: false,
        minimize: false,
        minimizer: [
            new TerserPlugin({
                sourceMap: true,
                terserOptions: {
                    compress: {
                        keep_fnames: true,
                        keep_classnames: true
                    },
                    mangle: {
                        keep_fnames: true,
                        keep_classnames: true
                    }
                }
            })
        ]
    }
};
